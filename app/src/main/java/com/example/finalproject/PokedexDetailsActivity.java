package com.example.finalproject;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.models.Pokemon;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PokedexDetailsActivity extends AppCompatActivity {

    // !! Instance Variables //
    public static final String TAG = "PokedexDetailsActivity";
    public static final String EXTRA_POKEMON_ID = "pokemonId";
    Pokemon pokemon;
    Taskable testDa; //Previously CSVTaskDataAccess //SQLTaskDataAccess
    TextView txtId;
    TextView txtName;
    TextView txtDescription;
    TextView txtType0;
    TextView txtType1;
    TextView txtDate;
    ImageView imgPokemon;
    Button btnSave;
    Button btnGetImage;
    Button btnReturn;
    SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
    //  END Instance Variables //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex_details);

        // !! Get a handle on the UI Views
        txtId = findViewById(R.id.textViewPokemonId);
        txtName = findViewById(R.id.textViewName);
        txtDescription = findViewById(R.id.textViewDescription);
        txtType0 = findViewById(R.id.textViewType0);
        txtType1 = findViewById(R.id.textViewType1);
        txtDate = findViewById(R.id.textViewDate);
        imgPokemon = findViewById(R.id.imgPokemon);

        btnReturn = findViewById(R.id.btnReturn);
        testDa = new SQLPokemonDataAccess(this);
        Intent i = getIntent();
        long id = i.getLongExtra(EXTRA_POKEMON_ID, 0);

        if(id > 0){
            pokemon = testDa.getPokemonById(id);
            Log.d(TAG, pokemon.toString());
            putDataIntoUI();
        }else{
            Log.d(TAG, "Creating a new Pokemon..."); // If a Pokemon isn't loaded to display then you have the option to create a Pokemon
        }


        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PokedexDetailsActivity.this, PokedexListActivity.class);
                startActivity(i);
            }
        });//
    }

    public void putDataIntoUI(){
        if(pokemon != null){
            try{
                String dateStr = sdf.format(pokemon.getDate());
                String idString = String.valueOf(pokemon.getId());
                txtId.setText(idString);
                txtName.setText(pokemon.getName());
                txtDescription.setText(pokemon.getDescription());
                txtType0.setText(pokemon.getType0());
                txtType1.setText(pokemon.getType1());
                txtDate.setText(dateStr);

            }catch(Exception e){
                Log.d(TAG, "ERROR: " + e.toString());
            }
        }
    }// End of putDataIntoUI()



}// End of PokemonDetailsActivity