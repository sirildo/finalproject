package com.example.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.models.Pokemon;

import java.util.ArrayList;

public class PokemonListActivity extends AppCompatActivity {

    public static final String TAG = "PokemonListActivity";
    ListView lsPokemon;
    Button btnAdd;
    Button btnReturn;
    SQLPokemonDataAccess testDa;
    ArrayList<Pokemon> allPokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        testDa = new SQLPokemonDataAccess(this);
//
////The reason I hardcoded this is because I wanted the Pokedex to have some existing Pokemon to go off of.
//
//        Pokemon localPokemon0 = new Pokemon(1, "Miltank", "Miltank is vital to our region's dairy industry. Its milk is delicious and chock-full of nutrients. However, if you drink too much, it could make your stomach hurt.", "Normal", "", new Date("August 15, 2000 00:00:00"), false); //8/15/2000
//        Pokemon localPokemon1 = new Pokemon(2, "Sandile", "This crocodile-like Pokemon isn't native, no one knows why it appears here. Many attribrute the changing climate for its lack of evolution.", "Ground", "Dark",  new Date("March 4, 2011 00:00:00"),false); //3/4/2011
//        Pokemon localPokemon2 = new Pokemon(3, "Barboach", "Considered a pest in our waters, trainers are encouraged to catch and release them into sanctuaries where they can't invade other bodies of water.", "Water", "Ground", new Date("March 19, 2003 00:00:00"),false); //3/19/2003
//        Pokemon localPokemon3 = new Pokemon(4, "Whiscash", "The evolution of Barboach. Unlike Barboach, Whiscash are a popular seafood in the area. Laws are in place to prevent over fishing.", "Water","Ground", new Date("March 19, 2003 00:00:00"), false); //3/19/2003
//        Pokemon localPokemon4 = new Pokemon(5, "Castform", "This Pokemon has slowly increased in numbers over the years. It's type changes based on the weather, and the weather can be unpredictable.","Normal","", new Date("March 19, 2003 00:00:00"),false); //4/19/2003
//        Pokemon localPokemon5 = new Pokemon(6, "Glameow", "Native to Sinnoh, Glameow is a popular foreign pet in the area. When angry, it flashes its claws, but it also can purr quite affectionately. ","Normal","",  new Date("April 22, 2007 00:00:00"),false); //4/22/2007
//        Pokemon localPokemon6 = new Pokemon(7, "Purugly", "The evolution of Glameow. Purugly is more assertive and independant than Glameow, because of this some trainer will abondon it at Goose Island.","Normal","",  new Date("April 22, 2007 00:00:00"),false); //4/22/2007
//        Pokemon localPokemon7 = new Pokemon(8, "Koffing", "Koffing started to appear during the Coulee Region's industrial era. You know when one is close, the smell and sound is hard to miss.", "Poison","", new Date("September 28, 1998 00:00:00"),false); //9/28/1998
//        Pokemon localPokemon8 = new Pokemon(9, "Wheezing", "The evolution of Koffing. Wheezing is commonly found near the city brewery and water treament center. It thrives off the odors produced by both.","Poison","", new Date("September 28, 1998 00:00:00"),false); //9/28/1998
//        Pokemon localPokemon9 = new Pokemon(10,"Tornadus", "Local legend says that this Pokemon protects the region from tornados and storms. Research suggets Tornadus actually causes them.", "Flying","", new Date("March 4, 2011 00:00:00"),false); //
//
//
//        localPokemon0 = testDa.insertPokemon(localPokemon0);
//        localPokemon1 = testDa.insertPokemon(localPokemon1);
//        localPokemon2 = testDa.insertPokemon(localPokemon2);
//        localPokemon3 = testDa.insertPokemon(localPokemon3);
//        localPokemon4 = testDa.insertPokemon(localPokemon4);
//        localPokemon5 = testDa.insertPokemon(localPokemon5);
//        localPokemon6 = testDa.insertPokemon(localPokemon6);
//        localPokemon7 = testDa.insertPokemon(localPokemon7);
//        localPokemon8 = testDa.insertPokemon(localPokemon8);
//        localPokemon9 = testDa.insertPokemon(localPokemon9);

//        testPokemon = testDa.insertPokemon(testPokemon);
//
//        ArrayList<Pokemon> testAllPokemon = testDa.getAllPokemon();
//        for(Pokemon t : testAllPokemon){
//            Log.d(TAG, t.toString());
//        }


        /////

        lsPokemon = findViewById(R.id.lsPokemon);
        btnAdd = findViewById(R.id.btnAdd);
        btnReturn = findViewById(R.id.btnReturn);

        //Only custom Pokemon can be edited, Pokemon whose rw i 'false' won't appear here
        allPokemon = testDa.getAllPokemon();


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PokemonListActivity.this, PokemonEditDetailsActivity.class);
                startActivity(i);
            }
        });

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PokemonListActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        ArrayAdapter<Pokemon> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, allPokemon);
        lsPokemon.setAdapter(adapter);
        lsPokemon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pokemon selectedPokemon = allPokemon.get(position);

                Log.d(TAG, "SELECTED POKEMON: " + selectedPokemon.toString());
                if(selectedPokemon.isRw()){
                    Intent i = new Intent(PokemonListActivity.this, PokemonEditDetailsActivity.class);
                    i.putExtra(PokemonEditDetailsActivity.EXTRA_POKEMON_ID, selectedPokemon.getId());
                    startActivity(i);
                }else{
                    Toast.makeText(PokemonListActivity.this, "This Pokemon cannot be edited", Toast.LENGTH_SHORT).show();
                }

            }
        });//
    }//
}//