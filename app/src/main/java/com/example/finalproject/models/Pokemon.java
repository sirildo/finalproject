package com.example.finalproject.models;

import java.util.Date;

public class Pokemon {

    private long id;
    private String name;
    private String description;
    private String type0;
    private String type1;
    private Date date;


    public boolean isRw() {
        return rw;
    }

    public void setRw(boolean rw) {
        this.rw = rw;
    }

    private boolean rw;

    public void setType0(String type0) {
        this.type0 = type0;
    }

    public Pokemon(String name, String description, String type0, String type1, Date date, boolean rw){
        this.name = name;
        this.description = description;
        this.type0 = type0;
        this.type1 = type1;
        this.date = date;
        this.rw = rw;
    }

    public Pokemon(long id, String name, String description, String type0, String type1,  Date date, boolean rw){
        this(name, description, type0, type1, date, rw);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType0() {
        return type0;
    }

    public void setType1(String type0) {
        this.type0 = type0;
    }

    public String getType1() {
        return type1;
    }

    public void setType2(String type1) {
        this.type1 = type1;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString(){
        return String.format("Pokemon ID# %d \n" +
                             "Name  %s \n" +
                             "Description \n %s \n" +
                             "Type \n %s %s \n" +
                             "Date Caught  %s", id, name, description, type0, type1, date);
    }
}
