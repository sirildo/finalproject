package com.example.finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.models.Pokemon;

import java.util.ArrayList;

public class PokedexListActivity extends AppCompatActivity {

    public static final String TAG = "PokedexListActivity";
    ListView lsPokemon;
    Button btnReturn;

    SQLPokemonDataAccess testDa;

    ArrayList<Pokemon> allPokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex_list);
        testDa = new SQLPokemonDataAccess(this);
        lsPokemon = findViewById(R.id.lsPokemon);
        allPokemon = testDa.getAllPokemon();
        btnReturn = findViewById(R.id.btnReturn);


        ArrayAdapter<Pokemon> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, allPokemon);
        lsPokemon.setAdapter(adapter);
        lsPokemon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pokemon selectedPokemon = allPokemon.get(position);
                Log.d(TAG, "SELECTED POKEMON: " + selectedPokemon.toString());

                    Intent i = new Intent(PokedexListActivity.this, PokedexDetailsActivity.class);
                    i.putExtra(PokemonEditDetailsActivity.EXTRA_POKEMON_ID, selectedPokemon.getId());
                    startActivity(i);


            }
        });//

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PokedexListActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }//
}//