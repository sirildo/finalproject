package com.example.finalproject;

import com.example.finalproject.models.Pokemon;

import java.util.ArrayList;

public interface Taskable {

    public ArrayList<Pokemon> getAllPokemon();

    public Pokemon getPokemonById(long id);

    public Pokemon insertPokemon(Pokemon t);

    public Pokemon updatePokemon(Pokemon t);

    public void deletePokemon(Pokemon t);

    public long getMaxId();
}
