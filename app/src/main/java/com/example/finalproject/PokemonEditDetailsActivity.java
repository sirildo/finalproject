package com.example.finalproject;



import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.models.Pokemon;

public class PokemonEditDetailsActivity extends AppCompatActivity {

    // !! Instance Variables //
    public static final String TAG = "PokemonDetailsActivity";
    public static final String EXTRA_POKEMON_ID = "pokemonId";
    Date date = new Date();
    Pokemon pokemon;
    Pokemon nullPokemon = new Pokemon(0, "N/A", "N/A", "type0", "type1", date, true);
    ArrayList<Pokemon> allPokemon;
    Taskable testDa; //Previously CSVTaskDataAccess //SQLTaskDataAccess
    EditText txtName;
    EditText txtDescription;
    EditText txtType1;
    EditText txtType2;
    EditText txtDate;
    ImageView imgPokemon;
    Button btnSave;
    Button btnGetImage;
    Button btnReturn;
    Button btnDelete;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy");
    //  END Instance Variables //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);
        testDa = new SQLPokemonDataAccess(this);

        // !! Get a handle on the UI Views
        txtName = findViewById(R.id.txtName);
        txtDescription = findViewById(R.id.txtDescription);
        txtType1 = findViewById(R.id.txtType0);
        txtType2 = findViewById(R.id.txtType1);
        txtDate = findViewById(R.id.txtDate);
        imgPokemon = findViewById(R.id.imgPokemon);

        btnSave = findViewById(R.id.btnSave);
        btnGetImage = findViewById(R.id.btnGenerateImage);
        btnReturn = findViewById(R.id.btnReturn);
        btnDelete = findViewById(R.id.btnDelete);

        allPokemon = testDa.getAllPokemon();

        Intent i = getIntent();
        long id = i.getLongExtra(EXTRA_POKEMON_ID, 0);

        if(id > 0){
            pokemon = testDa.getPokemonById(id);
            Log.d(TAG, pokemon.toString());
            putDataIntoUI();
        }else{
            Log.d(TAG, "Creating a new Pokemon..."); // If a Pokemon isn't loaded to display then you have the option to create a Pokemon
            pokemon = nullPokemon;
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "[SAVE] BUTTON CLICKED ( ! )");

                if(validate()) {
                    Log.d(TAG, "PASSED VALIDATION");

                    if (pokemon.getId() == 0) {
                        Log.d(TAG, "ATTEMPTING TO CREATE NEW POKEMON...");

                        getDataFromUI();

                        testDa.insertPokemon(pokemon);

                        } else {

                        Log.d(TAG, "Pokemon EXISTS. UPDATING Pokemon WITH NEW INFORMATION...");

                        getDataFromUI();
                        testDa.updatePokemon(pokemon);


                    }



                    Intent i = new Intent(PokemonEditDetailsActivity.this, PokemonListActivity.class);
                    startActivity(i);
                }
            }// End of onClick()
        });// End of btnSave.setOnClickListener()

        btnGetImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PokemonEditDetailsActivity.this, PokemonListActivity.class);
                startActivity(i);
            }
        });//

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "[DELETE] BUTTON CLICKED ( ! )");

                if(pokemon != null){
                    testDa.deletePokemon(pokemon);
                    Intent i = new Intent(PokemonEditDetailsActivity.this, PokemonListActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(PokemonEditDetailsActivity.this, "Nothing to delete", Toast.LENGTH_SHORT).show();
                }


            }// End of onClick()
        });// End of btnSave.setOnClickListener()
    }

    public void putDataIntoUI(){
        if(pokemon != null){
            try{
                String dateStr = sdf.format(pokemon.getDate());

                txtName.setText(pokemon.getName());
                txtDescription.setText(pokemon.getDescription());
                txtType1.setText(pokemon.getType0());
                txtType2.setText(pokemon.getType1());
                txtDate.setText(dateStr);

            }catch(Exception e){
                Log.d(TAG, "ERROR: " + e.toString());
            }
        }
    }// End of putDataIntoUI()

    private boolean validate(){
        boolean isValid = true;
        // !! validate the name
        if(txtName.getText().toString().isEmpty()){
            txtName.setError("You must enter a name");

            isValid = false;
        }
        // !! validate the description
        if(txtDescription.getText().toString().isEmpty()){
            txtDescription.setError("You must enter a description");

            isValid = false;
        }
        // !! validate the due date
        Date dueDate = null;
        if(txtDate.getText().toString().isEmpty()){
            txtDate.setError("You must enter a date");
            isValid = false;
        }else{
            try {
                dueDate = sdf.parse(txtDate.getText().toString());
            }catch (Exception e){
                txtDate.setError("Enter a valid date in M/d/yyyy");
                isValid = false;
                Log.d(TAG, "UNABLE TO CONVERT STRING TO DATE" + e.toString());
            }
        }
        // !! validate the first type
        if(txtType1.getText().toString().isEmpty()){
            txtType1.setError("Your Pokemon must have at least one type");
            isValid = false;
        }
        if(!pokemon.isRw()){
            isValid = false;
            Toast.makeText(this, "This Pokemon can't be edited", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }// End of validate()

    private void getDataFromUI(){

        String dateString = txtDate.getText().toString();
        Date date = null;

        try{
            date = sdf.parse(dateString);
        }catch (Exception e){
            Log.d(TAG, "UNABLE TO PARSE DATE STRING");
        }

        pokemon.setDescription(txtDescription.getText().toString());
        pokemon.setName(txtName.getText().toString());
        pokemon.setType1(txtType1.getText().toString());
        pokemon.setType2(txtType2.getText().toString());
        pokemon.setDate(date);



    }// End of getDataFromUI()



}// End of TaskDetailsActivity