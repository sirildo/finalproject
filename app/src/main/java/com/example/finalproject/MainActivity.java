package com.example.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
//TODO: finish activity layouts
// todo:  make images for buttons
// TODO: WISHLIST: replace MainActivity title text with an image of the title in a pokemon stylized font

//TODO: make a SQLite database file.
    //Create table cr_pokedex
    //TODO:-> pk_id, pk_name, pk_location,

public class MainActivity extends AppCompatActivity {

    Button btnPokedex;
    Button btnAddPokemon;
    // create a static constants for tagging our log entries
    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPokedex = findViewById(R.id.btnPokedex);

        btnPokedex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PokedexListActivity.class);
                startActivity(i);
            }
        });//

        btnAddPokemon = findViewById(R.id.btnAddPokemon);

        btnAddPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PokemonListActivity.class);
                startActivity(i);
            }
        });//
    }
}