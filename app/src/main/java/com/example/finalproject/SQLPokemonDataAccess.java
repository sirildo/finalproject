package com.example.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.finalproject.fileio.FileHelper;
import com.example.finalproject.models.Pokemon;
import com.example.finalproject.sqlite.MySQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SQLPokemonDataAccess implements Taskable {

    public static final String TAG = "SQLPokemonDataAccess";
    public static final String DATA_FILE = "pokemon.sqlite";

    //We need to  create statics for the table name and table columns
    public static final String TABLE_NAME = "pokemon";
    public static final String COLUMN_POKEMON_ID = "_id";
    public static final String COLUMN_POKEMON_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPE_0 = "type0";
    public static final String COLUMN_TYPE_1 = "type1";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_RW = "rw";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT )",
            TABLE_NAME,
            COLUMN_POKEMON_ID,
            COLUMN_POKEMON_NAME,
            COLUMN_DESCRIPTION,
            COLUMN_TYPE_0,
            COLUMN_TYPE_1,
            COLUMN_DATE,
            COLUMN_RW
    );


    //Instance variables
    private ArrayList<Pokemon> allPokemon = new ArrayList();;
    private Context context;
    private MySQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");

    //Constructors
    public SQLPokemonDataAccess(Context context){
        Log.d(TAG, "Instantiating SQLDataAccess");
        this.context = context;
        this.dbHelper = new MySQLiteOpenHelper(context);
        this.database = dbHelper.getWritableDatabase(); //Use getReadableDatabase() to only allow SELECT statements
    }

    @Override
    public ArrayList<Pokemon> getAllPokemon() {
        String query = String.format("SELECT %s, %s, %s, %s, %s, %s, %s FROM %s",
                COLUMN_POKEMON_ID,
                COLUMN_POKEMON_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_TYPE_0,
                COLUMN_TYPE_1,
                COLUMN_DATE,
                COLUMN_RW,
                TABLE_NAME
                );

        Cursor c = database.rawQuery(query, null);

        if(c != null && c.getCount() > 0){
            c.moveToFirst();
            while(!c.isAfterLast()){
                long id = c.getLong(0); //Might use int
                String name = c.getString(1);
                String desc = c.getString(2);
                String type0 = c.getString(3);
                String type1 = c.getString(4);
                String date = c.getString(5);
                String rw = c.getString(6);

                Date convertedDate = null;
                try{
                    convertedDate = dateFormat.parse(date);
                }catch(Exception e){
                    e.printStackTrace();
                    Log.d(TAG, "COULD NOT PARSE DATE!");
                }

                boolean convertedRw = Boolean.parseBoolean(rw);


                Pokemon t = new Pokemon(id, name, desc, type0, type1, convertedDate, convertedRw);
                allPokemon.add(t);
                c.moveToNext(); //DO NOT FORGET THIS LINE!!
            }
            c.close();
        }
        return allPokemon;
    }



    @Override
    public Pokemon getPokemonById(long id) {

        for(Pokemon p : allPokemon){
            if(p.getId() == id){
                return p;
            }
        }
        return null;
    }

    @Override
    public Pokemon insertPokemon(Pokemon p) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_POKEMON_NAME, p.getName());
        values.put(COLUMN_DESCRIPTION, p.getDescription());
        values.put(COLUMN_TYPE_0, p.getType0());
        values.put(COLUMN_TYPE_1, p.getType1());
        values.put(COLUMN_DATE, dateFormat.format(p.getDate()));
        values.put(COLUMN_RW, "true");
        long insertID = database.insert(TABLE_NAME, null, values);
        p.setId(insertID);
        return p;
    }

    @Override
    public Pokemon updatePokemon(Pokemon p) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_POKEMON_ID, p.getId());
        values.put(COLUMN_POKEMON_NAME, p.getName());
        values.put(COLUMN_DESCRIPTION, p.getDescription());
        values.put(COLUMN_TYPE_0, p.getType0());
        values.put(COLUMN_TYPE_1, p.getType1());
        values.put(COLUMN_DATE, dateFormat.format(p.getDate()));
        values.put(COLUMN_RW, "true");
        String whereClause = COLUMN_POKEMON_ID + " == " + p.getId();
        database.update(TABLE_NAME, values, whereClause, null );
        return p;
    }

    @Override
    public void deletePokemon(Pokemon p) {
        String whereClause = COLUMN_POKEMON_ID + " == " + p.getId();
        database.delete( TABLE_NAME, whereClause, null);
    }




    private void savePokemon(){

        String query = String.format("INSERT INTO %s VALUES %s, %s, %s, %s, %s, %s, %s",
                TABLE_NAME,
                COLUMN_POKEMON_ID,
                COLUMN_POKEMON_NAME,
                COLUMN_DESCRIPTION,
                COLUMN_TYPE_0,
                COLUMN_TYPE_1,
                COLUMN_DATE,
                COLUMN_RW

        );

    }

    public long getMaxId(){
        long maxId = 0;
        allPokemon = getAllPokemon();
        for(Pokemon p : allPokemon){
            if(p.getId() > maxId){
                maxId = p.getId();
            }
        }
        return maxId;
    }

}

