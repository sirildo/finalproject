package com.example.finalproject.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.finalproject.SQLPokemonDataAccess;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteHelper";
    private static final String DATA_BASE_NAME = "pokemon.sqlite";
    private static final int DATA_BASE_VERSION = 1;

    public MySQLiteOpenHelper(Context context){
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //String sql = "CREATE TABLE blah (_id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT, due TEXT, done INTEGER)";
        //insert row into the table
        //String insertSql = "INSERT INTO blah (description, due, done) VALUES ('foo', 'bar', 7)";
        //db.execSQL(insertSql);

        String sql = SQLPokemonDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, sql);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Upgrading Database");
        Log.d(TAG, "OLD VERSION: " + oldVersion);
        Log.d(TAG, "NEW VERSION: " + newVersion);

        switch(oldVersion){
            case 1:
                Log.d(TAG, "Upgrading from version 1");
//                String sql = "ALTER TABLE blah ADD new_column TEXT;";
//                db.execSQL(sql);
            case 2:
                Log.d(TAG, "Upgrading from version 2");
//                sql = "ALTER TABLE blah ADD new_column_2 TEXT;";
//                db.execSQL(sql);
        }
    }
}
